#define html HTML
#define htm HTML
#define js HTML
#define css HTML

#define mov QuickTime

#define mpeg Video
#define mpg Video
#define wma Video
#define avi Video

#define mp3 Sound
#define ogg Sound
#define wma Sound
#define wav Sound
#define mod Sound
#define xm Sound
#define s3m Sound
#define it Sound

#define doc WordProcessing
#define xsl WordProcessing
#define sxw WordProcessing

#define txt Text

#define c C
#define h H
#define cpp CPP
#define cc CPP
#define o O

#define java Java
#define jsp Java
#define jar Java
#define war Java

#define jpeg Image
#define jpg Image
#define bmp Image
#define gif Image
#define png Image
#define tga Image

#define rpm RPM

#define pdf PDF

#define zip Zip
#define bz2 Zip
#define gz Zip
#define tar Zip
#define tgz Zip
